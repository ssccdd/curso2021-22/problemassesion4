﻿[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 4

Problemas propuestos para la Sesión 4 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2021-22.

En estos ejercicios se utilizarán las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas. Las tareas se definirán como clases que implementen la interface [`Callable<V>`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Callable.html). 

Los **objetivos** de la práctica son:
-   Definir adecuadamente las clases que implementan lo que se ejecutarán en los marcos de ejecución. Programando adecuadamente la excepción de interrupción de su ejecución.
- Crear los marcos de ejecución que se necesiten y añadir las tareas para su ejecución así como la sincronización que se pida utilizando las capacidades del marco de ejecución para ello.
-   Modificar el método `main(..)` de la aplicación Java para una prueba correcta de las clases previamente definidas.
    -   Realizará la prueba de las tareas definidas en el ejercicio y la finalización correcta de los diferentes marcos de ejecución que sean necesarios.
    - Debe garantizar que todas las tareas están finalizadas antes de dar por terminada su ejecución.


Los ejercicios son diferentes para cada grupo:
-   [Grupo 1](https://gitlab.com/ssccdd/curso2021-22/problemassesion4/-/blob/master/README.md#grupo-1)
-   [Grupo 2](https://gitlab.com/ssccdd/curso2021-22/problemassesion4/-/blob/master/README.md#grupo-2)
-   [Grupo 3](https://gitlab.com/ssccdd/curso2021-22/problemassesion4/-/blob/master/README.md#grupo-3)
-   [Grupo 4](https://gitlab.com/ssccdd/curso2021-22/problemassesion4/-/blob/master/README.md#grupo-4)
-   [Grupo 5](https://gitlab.com/ssccdd/curso2021-22/problemassesion4/-/blob/master/README.md#grupo-5)
-   [Grupo 6](https://gitlab.com/ssccdd/curso2021-22/problemassesion4/-/blob/master/README.md#grupo-6)

### Grupo 1
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. La herramienta de sincronización será la que proporciona el marco de ejecución. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **GestorPedidos**: Esta tarea simulará la preparación de pedidos para unas personas compartidas entre los gestores.
    -   _Atributos_
	    -   Nombre del gestor.
	    -   Lista compartida con el hilo principal para las personas a las que se le prepara el pedido.
    
    -   _Ejecución de la tarea_
        -   Crea un pedido para cada una de las personas disponibles en la lista.
        -   Crea un marco de ejecución para los reponedores.
        -   Mientras no termine o sea interrumpido:
            -   Para cada pedido se selecciona aleatoriamente un tipo de producto.
            -   Se crea un **Reponedor** por cada tipo de producto y esperará a que todos ellos finalicen.
            -   Se asigna cada producto al pedido. Si no es posible se registra el producto para ese pedido como fallo.
            -   Asignar el producto al pedido se simulará con un tiempo de operación que estará comprendido entre 1 y 3 segundos.
            -   Si no puede asignar la mitad de los productos dará por concluido el trabajo.
        -   El resultado de la ejecución será una lista con los pedidos preparados y el registro de los fallos de productos no asignados.
        -   Debe implementarse la interrupción de la tarea y antes de finalizar deberá finalizar los reponedores que estén activos.
-   **Resultado**  : hay que definir una clase que sea el resultado de la ejecución de un gestor. Hay que sobreescribir el método  _toString()_.
-   **Reponedor** : Será el proceso encargado de entregar productos a sus gestores
    -   _Atributos_
        -   Nombre
        -   Tipo de producto
    -   _Ejecución_
        -   El resultado de su ejecución será el tipo de producto solicitado.
        -   Preparar el producto necesita un tiempo de entre 1 y 2 segundos.
        -   Deberá programarse su interrupción-
-   **HiloPrincipal**:
	-   Crea el marco de ejecución para los gestores.
	-   Crea tres **GestorPedidos** y los añade al marco de ejecución. Se deben crear las variables compartidas necesarias para estos gestores.
	-   El hilo principal creará un número de personas para cada gestor que estará comprendido entre 5 y 7 con recursos variables entre 40 y 60.
	-   Esperará a que finalice el primero de ellos.
	-   Solicita la interrupción del resto de gestores y espera hasta que finalicen.
	-   Antes de finalizar presentar el resultado de ejecución del gestor.

### Grupo 2
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. La herramienta de sincronización que se utilizará en el ejercicio la que proporciona el marco de ejecución. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **GestorProcesos**: Esta tarea simulará una gestión básica de procesos atendiendo a su uso de memoria.
    -   _Atributos_
        -   Identificador del gestor.
    -   _Ejecución_
        -   Creará el marco de ejecución para los generadores.
        -   Creará un **RegistroMemoria** con un número de páginas aleatorio entre 15 y 30.
        -   Repetir hasta que finalice o se solicite su interrupción:
            -   Creará un número de generadores entre 2 y 4 y esperará a que uno de ellos finalice su ejecución.
            -   Mientras tenga procesos que le haya proporcionado el generador que ha terminado:
                
                -   Asignará un proceso al registro de memoria si es posible. En otro caso se almacenará en una lista de rechazados.
                
                -   Con una probabilidad entre el 30% y 60% terminará la ejecución de un proceso. Los procesos que han terminado también deberán quedar registrados.
                
                -   Se simulará un tiempo de ejecución para este procedimiento que estará comprendido entre 2 y 4 segundos.
            -   Finalizará cuando haya asignado un número de procesos entre 15 y 20 a su registro de memoria.
        -   El resultado de la ejecución es el registro de memoria, los procesos no asignados y los procesos que han terminado.
        -   Antes de finalizar debe interrumpir los generadores y esperar a que finalicen.
-   **Resultado**  : hay que definir la la clase para el resultado de ejecución del gestor. Se debe implementar el método  **toString()**.
-   **GeneradorProcesos** : será el encargado de ir creando una serie de procesos para los gestores que se lo solicitan
    -   _Atributos_
        -   Nombre
    -   _Ejecución_
        -   Genera un número de procesos entre 3 y 5 de tipos variables.  
            
        -   La generación de un proceso requiere un tiempo entre 1 y 2 segundos.
        -   Debe programarse la interrupción.
-   **Hilo Principal**:
    -   Creará el marco de ejecución para los gestores.
    -   Se crearán 4 gestores y las variables compartidas necesarias. Se añadirán al marco de ejecución
    -   Esperará a que finalicen todos los gestores.
    -   Antes de finalizar se presentará el resultado de la ejecución de cada uno de los gestores.
### Grupo 3
Se deberán completar una serie de pedidos para unos clientes que tendrán una disponibilidad de recursos limitada. La herramienta de sincronización que se utilizará en el ejercicio son las que están disponibles mediante el marco de ejecución. Para ello se deberán realizar las siguientes tareas:  

-   Añadir todas las constantes necesarias en la interface **Constantes** para la resolución del ejercicio.
-   **Proveedor**: Será una tarea que simula el trabajo de un operador que estará completando una serie de ordenadores según una lista de componentes.
    -   _Atributos_
        -   Nombre del proveedor.
    -   _Ejecución_
        -   Creará entre 6 y 10 ordenadores.
        -   Mientras no complete al menos el 80% de los ordenadores o sea interrumpido:
            
            -   Tendrá un marco de ejecución para la ejecución de la tarea de los fabricantes.
            -   Creará un fabricante por cada uno de los tipos de componentes que aún necesite para completar los ordenadores.
            -   Esperará a que todos los fabricantes avisen que han completado su ejecución.
            -   Asignará componentes a los ordenadores hasta completar todos los componentes suministrados.
            
            -   Asignar el componente a un ordenador se simulará con un tiempo de operación que estará comprendido entre 1 y 3 segundos.
        -   Cuando finalice deberá devolver un  **Resultado** que incluye la lista de ordenadores que ha preparado y un alista de componentes que no ha incluido en ninguno de los ordenadores.
        -   Deberá programarse la interrupción de la tarea, si eso ocurre deberá finalizar cualquier fabricante activo antes de finalizar.
-   **Resultado**  : Diseñar esta clase para poder almacenar el resultado de ejecución de un proveedor. Hay que implementar el método  _toString()_.
-   **Fabricante** : simula la construcción de componentes para un ordenador
    -   _Atributos_
        -   Nombre.
        -   Tipo componente.
    -   _Ejecución_
        -   Crea un número de componentes, según el tipo asignado, de entre 2 y 4.
        -   Cada uno de los componentes requiere un tiempo de creación entre 1 y 3 segundos.
        -   También debe implementarse la interrupción de la tarea.
-   **HiloPrincipal**:
    -   Crear el marco de ejecución para los proveedores.
    -   Crear entre 3 y 5 **Proveedor** y los añade al marco de ejecución. Se deben crear las variables compartidas necesarias para estos proveedores.
    -   Esperará a que finalice uno de los proveedores.
    -   Interrumpirá el resto de proveedores.
    -   Antes de finalizar se debe presentar el resultado de ejecución del proveedor que ha finalizado.
    
### Grupo 4
Una sesión más continuamos con la misma temática. El ejercicio consiste en la creación de una serie de recursos por parte de la Universidad y la asignación a planes de estudio por parte de las plataformas de aprendizaje. Para evitar problemas de acceso simultáneo a una misma estructura de datos, continuaremos asociando una universidad con una plataforma de aprendizaje.

 - `Universidad`: su tarea es crear recursos de aprendizaje y enviarlos una vez haya terminado. Los recursos se siguen generando del mismo modo que en sesiones anteriores.
	 - Atributos:
		 - Identificador único.
	- Tareas:
		- Indica que ha comenzado a crear recursos.
		- Crea todos los recursos (`RECURSOS_A_GENERAR`).
		- Por cada recurso generado, el hilo espera un tiempo aleatorio dentro del intervalo [`TIEMPO_CREACION_RECURSOS_MIN`, `TIEMPO_CREACION_RECURSOS_MAX`]. El tiempo está en milisegundos.
		- Indica que ha terminado de crear recursos.
		- Devuelve el resultado de todo el proceso (recursos generados).
- `PlataformaAprendizaje`: su tarea es asignar recursos a los planes de estudios que se le proporcionen.
	- Atributos:
		- Identificador único.
		- Recursos que tiene que asignar.
		- Recursos que no se han podido asignar.
		- Planes de estudio.
	- Tareas:
		- Indica que ha comenzado a organizar recursos.
		- Organiza los recursos en planes de estudio. Para ello, cada recurso, debemos intentar añadirlo a un plan de estudios. En el momento que hemos conseguido añadir el recurso, pasamos al siguiente, y en el caso, de que no se pueda añadir en ninguno, lo descartamos.
		- Por cada recurso organizado, el hilo espera un tiempo aleatorio dentro del intervalo [`TIEMPO_ASIGNACION_RECURSOS_MIN`, `TIEMPO_ASIGNACION_RECURSOS_MAX`]. El tiempo está en milisegundos.
		- Indica que ha terminado de organizar recursos.
		- Devuelve un `Informe` con los resultados.
- `Informe`: su función es simplemente presentar unos datos.
	- Atributos:
		- Identificador del hilo plataforma.
		- Número de planes de estudio completados.
		- Número de planes de estudio sin completar.
		- Porcentaje de recursos añadidos al plan de estudios.
	- Métodos:
		- toString(): presentar en lenguaje natural los datos que hemos recogido en el informe. 
- `Hilo principal`: 
	- Lanzamos las universidades (`UNIVERSIDADES_A_GENERAR`) a través de un `ExecutorService`, limitando el número máximo de hilos que ejecuta de manera simultánea a 3.
	- Esperamos a que todos los hilos nos devuelvan los recursos generados.
	- Asignamos a cada plataforma los recursos de su universidad.
	- Lanzamos las plataformas (`PLATAFORMAS_A_GENERAR`) a través del `ExecutorService`, y no hacemos ningún tipo de limitación.
	- En este caso, obtenemos el resultado del primer hilo plataforma que termine y mostramos su informe.
	- Para terminar finalizamos los objetos `ExecutorService`.

**Nota**: se deben tratar adecuadamente las interrupciones. En caso de interrupción, el hilo deja de realizar su tarea y devuelve el resultado parcialmente (lo que le ha dado tiempo a completar). Si un hilo es interrumpido, imprímelo por pantalla.

### Grupo 5
Una sesión más continuamos con la misma temática. El ejercicio consiste en la creación de una serie de coches por parte de los depósitos y la asignación de estos a reservas por parte de los gestores. Para evitar problemas de acceso simultáneo a una misma estructura de datos, continuaremos asociando un depósito con un gestor.

 - `Deposito`: su tarea es crear coches y enviarlos una vez haya terminado. Los coches se siguen generando del mismo modo que en sesiones anteriores.
	 - Atributos:
		 - Identificador único.
	- Tareas:
		- Indica que ha comenzado a crear coches.
		- Crea todos los coches (`COCHES_A_GENERAR`).
		- Por cada coche generado, el hilo espera un tiempo aleatorio dentro del intervalo [`TIEMPO_ESPERA_PREPARACION_MIN`, `TIEMPO_ESPERA_PREPARACION_MAX`]. El tiempo está en milisegundos.
		- Indica que ha terminado de crear coches.
		- Devuelve el resultado de todo el proceso (coches generados).
- `Gestor`: su tarea es asignar coches a las reservas disponibles. Se ha añadido un nuevo método a la clase `Reserva` para comprobar que está completa.
	- Atributos:
		- Identificador único.
		- Coches que tiene que asignar.
		- Coches que no se han podido asignar.
		- Reservas.
	- Tareas:
		- Indica que ha comenzado a organizar coches.
		- Organiza los coches en reservas. Para ello, cada coche, debemos intentar añadirlo a una reserva. En el momento que hemos conseguido añadir la reserva, pasamos a la siguiente, y en el caso, de que no se pueda añadir en ninguna, lo descartamos.
		- Por cada coche organizado, el hilo espera un tiempo aleatorio dentro del intervalo [`TIEMPO_ESPERA_ASIGNACION_MIN`, `TIEMPO_ESPERA_ASIGNACION_MAX`]. El tiempo está en milisegundos.
		- Indica que ha terminado de organizar coches.
		- Devuelve un `Informe` con los resultados.
- `Informe`: su función es simplemente presentar unos datos.
	- Atributos:
		- Identificador del hilo plataforma.
		- Número de reservas completadas.
		- Porcentaje de reservas sin completar.
		- Número de coches que se han añadido a una reserva.
	- Métodos:
		- toString(): presentar en lenguaje natural los datos que hemos recogido en el informe. 
- `Hilo principal`: 
	- Lanzamos los depósitos (`DEPOSITOS_A_GENERAR`) a través de un `ExecutorService`, limitando el número máximo de hilos que ejecuta de manera simultánea a 4.
	- Esperamos a que todos los hilos nos devuelvan los coches generados.
	- Asignamos a cada gestor los coches de su depósito.
	- Lanzamos los gestores (`GESTORES_A_GENERAR`)  a través del `ExecutorService`, limitando el número máximo de hilos que ejecuta de manera simultánea a 2.
	- Esperamos a que todos los hilos nos devuelvan los informes generados.
	- Para terminar finalizamos los objetos `ExecutorService` que hemos creado.

**Nota**: se deben tratar adecuadamente las interrupciones. En caso de interrupción, el hilo deja de realizar su tarea y devuelve el resultado parcialmente (lo que le ha dado tiempo a completar). Si un hilo es interrumpido, imprímelo por pantalla.

### Grupo 6
Una sesión más continuamos con la misma temática. El ejercicio consiste en la creación de una serie de jugadores por parte de la clase `RegistroJugadores`  y la asignación a equipos por parte de los organizadores. Para evitar problemas de acceso simultáneo a una misma estructura de datos, continuaremos asociando un registrador con un organizador.

 - `RegistroJugadores`: su tarea es crear jugadores y enviarlos una vez haya terminado. Los jugadores se siguen generando del mismo modo que en sesiones anteriores.
	 - Atributos:
		 - Identificador único.
	- Tareas:
		- Indica que ha comenzado a crear jugadores.
		- Crea todos los jugadores (`JUGADORES_A_GENERAR`).
		- Por cada jugador generado, el hilo espera un tiempo aleatorio dentro del intervalo [`TIEMPO_CREACION_JUGADORES_MIN`, `TIEMPO_CREACION_JUGADORES_MAX`]. El tiempo está en milisegundos.
		- Indica que ha terminado de crear jugadores.
		- Devuelve el resultado de todo el proceso (jugadores generados).
- `Organizador`: su tarea es asignar jugadores a los equipos disponibles.
	- Atributos:
		- Identificador único.
		- Jugadores que tiene que asignar.
		- Jugadores que no se han podido asignar.
		- Equipos.
	- Tareas:
		- Indica que ha comenzado a organizar jugadores.
		- Organiza los jugadores en equipos. Para ello, cada jugador, debemos intentar añadirlo a un equipo. En el momento que hemos conseguido añadir el jugador, pasamos al siguiente, y en el caso, de que no se pueda añadir en ninguno, lo descartamos.
		- Por cada jugador organizado, el hilo espera un tiempo aleatorio dentro del intervalo [`TIEMPO_ASIGNACION_RECURSOS_MIN`, `TIEMPO_ASIGNACION_RECURSOS_MAX`]. El tiempo está en milisegundos.
		- Indica que ha terminado de organizar jugadores.
		- Devuelve un `Informe` con los resultados.
- `Informe`: su función es simplemente presentar unos datos.
	- Atributos:
		- Identificador del hilo organizador.
		- Número de equipos completados.
		- Número de jugadores que sí ha podido incluir en un equipo.
		- Porcentaje de jugadores que se han desechado (únicamente los jugadores que ha descartado el hilo que genera el informe, no el total).
	- Métodos:
		- toString(): presentar en lenguaje natural los datos que hemos recogido en el informe. 
- `Hilo principal`: 
	- Lanzamos los hilos registradores (`REGISTRADORES_A_GENERAR`) a través de un `ExecutorService`, limitando el número máximo de hilos que ejecuta de manera simultánea a 5.
	- Esperamos a que todos los hilos nos devuelvan los jugadores generados.
	- Asignamos a cada organizador los jugadores de su registrador.
	- Lanzamos los organizadores (`ORGANIZADORES_A_GENERAR`) a través del `ExecutorService`, y no hacemos ningún tipo de limitación.
	- En este caso, obtenemos el resultado del primer hilo organizador que termine y mostramos su informe.
	- Para terminar finalizamos los objetos `ExecutorService` que hemos creado.

**Nota**: se deben tratar adecuadamente las interrupciones. En caso de interrupción, el hilo deja de realizar su tarea y devuelve el resultado parcialmente (lo que le ha dado tiempo a completar). Si un hilo es interrumpido, imprímelo por pantalla.
