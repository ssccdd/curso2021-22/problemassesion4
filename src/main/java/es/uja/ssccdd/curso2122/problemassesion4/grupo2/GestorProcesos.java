/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.D100;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MAX_FIN_PROCESO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_FIN_PROCESO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_GENERADORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_PROCESOS_PARA_ASIGNAR;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.NINGUNO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.VARIACION_ASIGNACION_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.VARIACION_GENERADORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.VARIACION_PAGINAS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorProcesos implements Callable<Resultado> {
    private final String iD;
    private final RegistroMemoria registro;
    private final ArrayList<Proceso> procesosNoAsignados;
    private final ArrayList<Proceso> procesosFinalizados;
    private final ExecutorService ejecucion;
    private int procesosParaAsignar;

    public GestorProcesos(String iD) {
        this.iD = iD;
        this.procesosNoAsignados = new ArrayList();
        this.procesosFinalizados = new ArrayList();
        
        // Se crea el registro de memoria
        int paginas = MIN_PAGINAS + aleatorio.nextInt(VARIACION_PAGINAS);
        this.registro = new RegistroMemoria("Registro-" + iD, paginas);
        
        // Numero de procesos para asignar
        this.procesosParaAsignar = MIN_PROCESOS_PARA_ASIGNAR + 
                           aleatorio.nextInt(VARIACION_ASIGNACION_PROCESOS);
        
        // Marco ejecución para los generadores de procesos
        this.ejecucion = Executors.newCachedThreadPool();  
    }
    
    @Override
    public Resultado call() throws Exception {
        Resultado resultado = null;
        List<Proceso> listaProcesos;
        
        System.out.println("LA TAREA(" + iD + ") comienza su ejecución para asignar "
                            + procesosParaAsignar + " procesos");
        
        try {
            // Hasta su finalización o su interrupción
            do {
                // Esperamos que un generador tenga procesos
                System.out.println("LA TAREA(" + iD + ") espera por nuevos procesos");
                listaProcesos = esperarProcesos();
                
                asignarProcesos(listaProcesos);
                
            } while( procesosParaAsignar != NINGUNO );
            
            resultado = new Resultado("Resultado-" + iD, registro, procesosNoAsignados, procesosFinalizados);
            System.out.println("LA TAREA(" + iD + ") ha finalizado su ejecución");
        } catch(InterruptedException ex) {
            System.out.println("LA TAREA(" + iD + ") ha sido INTERRUMPIDO");
        } finally {
            // finalizamos los gestores activos y el marco de ejecución
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return resultado;
    }

    public String getiD() {
        return iD;
    }
    
    /**
     * Crea un número variable de generadores de procesos y espera a que termine el
     * primero
     * @return Lista de procesos del generador que ha finalizado
     * @throws InterruptedException
     * @throws ExecutionException 
     */
    private List<Proceso> esperarProcesos() throws InterruptedException, ExecutionException {
        int numGeneradores = MIN_GENERADORES + aleatorio.nextInt(VARIACION_GENERADORES);
        ArrayList<GeneradorProcesos> listaGeneradores = new ArrayList();
        
        for(int i = 0; i < numGeneradores; i++) {
            GeneradorProcesos generador = new GeneradorProcesos("Generador-" + iD + "-" + i);
            listaGeneradores.add(generador);
        }
        
        return ejecucion.invokeAny(listaGeneradores);
    }
    
    /**
     * Asignamos los procesos, si es posible, al registro de memoria y comprobamos
     * si ha finalizado alguno de ellos.
     * @param listaProcesos
     * @throws InterruptedException 
     */
    private void asignarProcesos(List<Proceso> listaProcesos) throws InterruptedException {
        int tiempo;
        Iterator it = listaProcesos.iterator();
               
        while( it.hasNext() && procesosParaAsignar != NINGUNO ) {
            // Simulamos el tiempo de operación
            tiempo = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
            TimeUnit.SECONDS.sleep(tiempo);
            
            Proceso proceso = (Proceso) it.next();
            if( registro.addProceso(proceso) ) {
                procesosParaAsignar--;
                System.out.println("LA TAREA(" + iD + ") añade el proceso " + proceso +
                                   " a su registro de memoria." +
                                   "\n\tQuedan por asignar " + procesosParaAsignar);
            } else {
                System.out.println("LA TAREA(" + iD + ") el proceso " + proceso +
                                       " NO AÑADIDO");
                procesosNoAsignados.add(proceso);
            }
            
            if( procesoFinalizado() ) {
                procesosFinalizados.add(registro.removeProceso());
                System.out.println("LA TAREA(" + iD + ") el proceso " + proceso +
                                       " ha DEJADO el registro de memoria");
            }
        }
    }
    
    private boolean procesoFinalizado() {
        int resultado = aleatorio.nextInt(D100);
        return (resultado > MIN_FIN_PROCESO) && (resultado < MAX_FIN_PROCESO);
    }
}
