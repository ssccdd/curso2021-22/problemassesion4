/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.FIN_EJECUCION;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.NINGUNO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.TIEMPO_ESPERA;
import es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.TipoProducto;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GestorPedidos implements Callable<Resultado> {
    private final String iD;
    private final ArrayList<Persona> listaPersonas;
    private final ArrayList<Pedido> listaPedidos;
    private final ArrayList<TipoProducto> productosNoAsignados;
    private final ExecutorService ejecucion;

    public GestorPedidos(String iD, ArrayList<Persona> listaPersonas) {
        this.iD = iD;
        this.listaPersonas = listaPersonas;
        this.listaPedidos = new ArrayList();
        this.productosNoAsignados = new ArrayList();
        this.ejecucion = Executors.newCachedThreadPool();
    }

    

    @Override
    public Resultado call() throws Exception {
        Resultado resultado = null;
        boolean finalTarea;
        List<Future<TipoProducto>> resultadoReponedores;
        
        System.out.println("LA TAREA(" + iD + ") comienza su ejecución para " + listaPersonas.size() +
                           " pedidos");

        crearPedidos();
        
        try {
            // Hasta su finalización o su interrupción
            do {
                System.out.println("LA TAREA(" + iD + ") pide reposición");
                resultadoReponedores = reponerProductos();
               
                System.out.println("LA TAREA(" + iD + ") empieza la asignación de productos");
                finalTarea = asignarProductos(resultadoReponedores);
            } while(!finalTarea);
            
            System.out.println("LA TAREA(" + iD + ") ha finalizado su ejecución");
            resultado = new Resultado("Resultado-" + iD, listaPedidos, productosNoAsignados);
            
        } catch(InterruptedException ex) {
            System.out.println("LA TAREA(" + iD + ") ha sido INTERRUMPIDO");
        } finally {
            finalizacion();
        }
        
        
        return resultado;   
    }

    public String getiD() {
        return iD;
    }
    
    
    private void finalizacion() throws InterruptedException {
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
    }
    
    private void crearPedidos() {
        for(Persona persona : listaPersonas) {
            listaPedidos.add(new Pedido("Pedido-" + persona.getNombre(), persona));
        }
    }
    
    /**
     * Se crea un reponedor con un tipo de producto aleatorio para cada uno de los
     * pedidos. Espera a que todos hayan finalizado
     * @return Lista de los resultados futuros de la ejecución.
     */
    private List<Future<TipoProducto>> reponerProductos() throws InterruptedException {
        List<Future<TipoProducto>> resultado;
        ArrayList<Reponedor> listaReponedores = new ArrayList();
        
        for(int i = 0; i < listaPedidos.size(); i++) {
            TipoProducto tipoProducto = TipoProducto.getTipoProducto();
            Reponedor reponedor = new Reponedor("Reponedor-" + i + " " + tipoProducto.name(),
                                                tipoProducto);
            listaReponedores.add(reponedor);
        }
        
        resultado = ejecucion.invokeAll(listaReponedores);
        
        return resultado;
    }
    
    /**
     * Asigna los productos hechos por los reponedores a los pedidos si es posible.
     * Si no se han podido asignar al menos la mitad de los productos se da por finalizada
     * la tarea.
     * @param resultadoReponedores resultado de la ejecución de los reponedores
     * @return si la tarea ha finalizado o no.
     * @throws InterruptedException
     * @throws ExecutionException 
     */
    private boolean asignarProductos(List<Future<TipoProducto>> resultadoReponedores) throws InterruptedException, ExecutionException {
        int falloAsignacion = NINGUNO;
        int tiempoProcesado;
        boolean finalTarea = !FIN_EJECUCION;
        int indice = PRIMERO;
        
        Iterator it = resultadoReponedores.iterator();
        while( it.hasNext() && !finalTarea) {
            Future<TipoProducto> resultadoReponedor = (Future<TipoProducto>) it.next();
            TipoProducto tipoProducto = resultadoReponedor.get();
            Pedido pedido = listaPedidos.get(indice);
            if( !pedido.addProducto(tipoProducto) ) {
                falloAsignacion++;
                productosNoAsignados.add(tipoProducto);  
                System.out.println("LA TAREA(" + iD + ") " + tipoProducto + " no ASIGNADO");
            } else {
                System.out.println("LA TAREA(" + iD + ") " + tipoProducto + " ASIGNADO a " +
                                   pedido.getID());
            }
            indice++;
            
            // Se simula un tiempo de procesamiento
            tiempoProcesado = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
            TimeUnit.SECONDS.sleep(tiempoProcesado);
        }
        
        if( falloAsignacion > (listaPedidos.size() / 2) )
            finalTarea = FIN_EJECUCION;
                
        return finalTarea;
    }
}
