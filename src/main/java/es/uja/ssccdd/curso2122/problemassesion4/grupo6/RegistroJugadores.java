package es.uja.ssccdd.curso2122.problemassesion4.grupo6;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class RegistroJugadores implements Callable<ArrayList<Jugador>> {

    // Atributos.
    private final int ID;

    public RegistroJugadores(int ID) {
        this.ID = ID;
    }

    private Jugador generaJugador(int currentID) {
        int valor_rol = Utils.random.nextInt(Utils.VALOR_GENERACION);
        return new Jugador(currentID, Utils.RolJugador.getRolJugador(valor_rol));
    }

    @Override
    public ArrayList<Jugador> call() {
        System.out.println("RegistroJugador " + this.ID + ": ha empezado a crear jugadores");

        ArrayList<Jugador> jugadoresGenerados = new ArrayList<>();
        try {   // Si nos interrumpen finalizamos la generación de jugadores,
                // y devolvemos los jugadores que hemos generado de manera parcial.

            // Generamos todos los recursos.
            for (int i = 0; i < Utils.JUGADORES_A_GENERAR; i++) {
                jugadoresGenerados.add(this.generaJugador(i + 1));

                // Simulamos un tiempo de creación.
                int tiempoAleatorio = Utils.random.nextInt(Utils.TIEMPO_CREACION_JUGADORES_MAX - Utils.TIEMPO_CREACION_JUGADORES_MIN) + Utils.TIEMPO_CREACION_JUGADORES_MIN;
                TimeUnit.MILLISECONDS.sleep(tiempoAleatorio);
            }
        } catch (InterruptedException e) {
            System.out.println(" " + this.ID + ": ha sido interrumpido");
        }

        System.out.println("RegistroJugador " + this.ID + ": ha terminado de crear jugadores");
        return jugadoresGenerados;
    }
}
