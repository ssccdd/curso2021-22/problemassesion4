/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.MIN_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.MIN_TIEMPO_FABRICACION;
import es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.TipoComponente;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.VARIACION_COMPONENTES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.VARIACION_TIEMPO_FABRICACION;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Fabricante implements Callable<List<Componente>> {
    private final String iD;
    private final TipoComponente tipoComponente;

    public Fabricante(String iD, TipoComponente tipoComponente) {
        this.iD = iD;
        this.tipoComponente = tipoComponente;
    }
   
    @Override
    public List<Componente> call() throws Exception {
        List<Componente> resultado = new ArrayList();
        int numeroComponentes;
        
        System.out.println("LA TAREA(" + iD + ") comienza su ejecución");
        
        try {
            
            numeroComponentes = fabricarComponentes(resultado);
            
            System.out.println("LA TAREA(" + iD + ") ha fabricado " + numeroComponentes +
                                " componentes " + tipoComponente);
        } catch(InterruptedException ex) {
            System.out.println("LA TAREA(" + iD + ") ha sido INTERRUMPIDO");      
        }
        
        
        return resultado;
    }
    
    public String getiD() {
        return iD;
    }
    
    private int fabricarComponentes(List<Componente> listaComponentes) throws InterruptedException {
        int tiempoFabricacion;
        int numeroComponentes;
        
        numeroComponentes = MIN_COMPONENTES + aleatorio.nextInt(VARIACION_COMPONENTES);
        for(int i = 0; i < numeroComponentes; i++) {
            listaComponentes.add(new Componente(iD + "-" + this.hashCode() + "-" + i, tipoComponente));
            
            // Simula el tiempo de fabricación
            tiempoFabricacion = MIN_TIEMPO_FABRICACION + aleatorio.nextInt(VARIACION_TIEMPO_FABRICACION);
            TimeUnit.SECONDS.sleep(tiempoFabricacion);
        }
        
        return numeroComponentes;
    }
}
