package es.uja.ssccdd.curso2122.problemassesion4.grupo4;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Universidad implements Callable<ArrayList<Recurso>> {

    // Atributos.
    private final int ID;

    public Universidad(int ID) {
        this.ID = ID;
    }

    private Recurso generaRecurso(int currentID) {
        int valor_tipo = Utils.random.nextInt(Utils.VALOR_GENERACION);
        int creditos = Utils.random.nextInt(Utils.CREDITOS_MAXIMOS_ASIGNATURA - Utils.CREDITOS_MINIMOS_ASIGNATURA) + Utils.CREDITOS_MINIMOS_ASIGNATURA;
        return new Recurso(currentID, Utils.TipoRecurso.getRecurso(valor_tipo), creditos);
    }

    @Override
    public ArrayList<Recurso> call() {
        System.out.println("Universidad " + this.ID + ": ha empezado a crear recursos");

        ArrayList<Recurso> recursosGenerados = new ArrayList<>();
        try {   // Si nos interrumpen finalizamos la generación de recursos,
                //y devolvemos los recursos que hemos generado de manera parcial.

            // Generamos todos los recursos.
            for (int i = 0; i < Utils.RECURSOS_A_GENERAR; i++) {
                recursosGenerados.add(this.generaRecurso(i + 1));

                // Simulamos un tiempo de creación.
                int tiempoAleatorio = Utils.random.nextInt(Utils.TIEMPO_CREACION_RECURSOS_MAX - Utils.TIEMPO_CREACION_RECURSOS_MIN) + Utils.TIEMPO_CREACION_RECURSOS_MIN;
                TimeUnit.MILLISECONDS.sleep(tiempoAleatorio);

            }
        } catch (InterruptedException e) {
            System.out.println("Universidad " + this.ID + ": ha sido interrumpido");
        }

        System.out.println("Universidad " + this.ID + ": ha terminado de crear recursos");
        return recursosGenerados;
    }
}
