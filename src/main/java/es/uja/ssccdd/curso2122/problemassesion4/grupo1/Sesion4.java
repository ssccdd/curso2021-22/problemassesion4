/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.INCREMENTO_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.MIN_PERSONAS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.MIN_RECURSOS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.VARIACION_PERSONAS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author José Luis López Ruiz (alluque)
 */
public class Sesion4 {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        ArrayList<GestorPedidos> listaGestores;
        ArrayList<Persona>[] listaPersonas;
        Resultado resultado;
        Persona persona;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        listaPersonas = new ArrayList[NUM_GESTORES];
        listaGestores = new ArrayList();
        ejecucion = Executors.newCachedThreadPool();
        for(int i = 0; i < NUM_GESTORES; i++) {
            listaPersonas[i] = new ArrayList();
            int numPersonas = MIN_PERSONAS + aleatorio.nextInt(VARIACION_PERSONAS);
            for(int j = 0; j < numPersonas; j++) {
                persona = new Persona("Persona-" + i + "-" + j, MIN_RECURSOS + 
                                                  aleatorio.nextInt(INCREMENTO_RECURSOS));
                listaPersonas[i].add(persona);
            }
            listaGestores.add(new GestorPedidos("Gestor-" + i, listaPersonas[i]));
        }
        
        
        // Se ejecutan los gestores
        System.out.println("Hilo(PRINCIPAL) se inician los GESTORES y espera a que termine el primero");
        resultado = ejecucion.invokeAny(listaGestores);
        
        System.out.println("Hilo(PRINCIPAL) solicita la INTERRUPCION de los gestores y espera a su finalización");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentar resultados
        System.out.println(resultado);
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
        
    }

}
