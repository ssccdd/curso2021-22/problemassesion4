/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.NUM_GESTORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.TIEMPO_ESPERA;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author José Luis López Ruiz (alluque)
 */
public class Sesion4 {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        ArrayList<GestorProcesos> listaGestores;
        List<Future<Resultado>> listaResultados;
        
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        ejecucion = Executors.newCachedThreadPool();
        listaGestores = new ArrayList();
        for(int i = 0; i < NUM_GESTORES; i++) {
            listaGestores.add(new GestorProcesos("Gestor-" + i));
        }
        
        // Cuerpo de ejecución del hilo principal
        System.out.println("Hilo(PRINCIPAL) comienza la ejecución de los gestores y espera a "
                + "que finalie el primero");
        listaResultados = ejecucion.invokeAll(listaGestores);
        
        // Finaliza el marco de ejecución
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentar resultados
        for(Future<Resultado> resultado : listaResultados)
            System.out.println(resultado.get());
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
}
