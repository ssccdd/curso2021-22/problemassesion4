/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo1;

import es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.TipoProducto;
import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Resultado {
    private final String iD;
    private final ArrayList<Pedido> listaPedidos;
    private final ArrayList<TipoProducto> listaProductos;

    public Resultado(String iD, ArrayList<Pedido> listaPedidos, ArrayList<TipoProducto> listaProductos) {
        this.iD = iD;
        this.listaPedidos = listaPedidos;
        this.listaProductos = listaProductos;
    }

    @Override
    public String toString() {
        String resultado  = "************ Lista Pedidos " + iD + " *******************";
        
        for(Pedido pedido : listaPedidos)
            resultado += "\n\t" + pedido;
        
        resultado += "\n Productos no asignados a ningún pedido\n\t" + listaProductos;
        resultado += "\n***************************************************************";
        
        return resultado;
    }
}
