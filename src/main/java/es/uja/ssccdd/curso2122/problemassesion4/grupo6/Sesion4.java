package es.uja.ssccdd.curso2122.problemassesion4.grupo6;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Sesion4 {

    public static void main(String[] args) {
        System.out.println("Hilo principal: Iniciando ejecución");
        System.out.println("------------------------------------");

        // Creamos todo lo necesario.
        ExecutorService executorRegistroJugadores = (ExecutorService) Executors.newFixedThreadPool(Utils.MAX_REGISTROJUGADORES_SIMULTANEAMENTE);

        // - Creación de hilos RegistroJugadores.
        List<RegistroJugadores> registroJugadores = new ArrayList<>();
        for (int i = 0; i < Utils.REGISTROJUGADORES_A_GENERAR; i++) {
            registroJugadores.add(new RegistroJugadores(i + 1));
        }

        // - Creación de hilos organizadores.
        ArrayList<Equipo> equipos = null;
        ArrayList<Jugador> jugadoresDesechados = new ArrayList<>();

        List<Organizador> organizadores = new ArrayList<>();
        for (int i = 0; i < Utils.ORGANIZADORES_A_GENERAR; i++) {
            // - Creación de equipos.
            equipos = new ArrayList<>();
            for (int j = 0; j < Utils.EQUIPOS_A_GENERAR; j++) {
                equipos.add(new Equipo(j + 1));
            }

            organizadores.add(new Organizador(i + 1, jugadoresDesechados, equipos));
        }

        // Lanzamos registradores y esperamos la devolución de los resultados.
        List<Future<ArrayList<Jugador>>> resultados = new ArrayList<>();
        try {
            resultados = executorRegistroJugadores.invokeAll(registroJugadores);
        } catch (InterruptedException e) {
            System.out.println("Ha ocurrido algún problema al esperar los resultados de los hilos universidad");
        }

        // Asignamos a cada organizador los jugadores de su registrador.
        ArrayList<Jugador> jugadores = null;
        for (int i = 0; i < resultados.size() && i < organizadores.size(); i++) {
            try {
                organizadores.get(i).setJugadoresDisponibles(resultados.get(i).get());
            } catch (ExecutionException | InterruptedException e) {
                System.out.println("Ha ocurrido algún problema al asignar los jugadores al organizador " + (i + 1));
            }
        }

        // Lanzamos organizadores.
        ExecutorService executorOrganizadores = (ExecutorService) Executors.newFixedThreadPool(Utils.ORGANIZADORES_A_GENERAR);
        Informe primer_resultado = null;
        try {
            primer_resultado = executorOrganizadores.invokeAny(organizadores);
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ha ocurrido algún problema al esperar el informe");
        }

        // Finalizamos la ejecución del executer.
        executorRegistroJugadores.shutdown();
        executorOrganizadores.shutdown();

        // Imprimimos el primer informe que se ha generado.
        if (primer_resultado != null)
            System.out.println(primer_resultado.toString());
 
        System.out.println("------------------------------------");
        System.out.println("Hilo principal: Terminando ejecución");
    }
}
