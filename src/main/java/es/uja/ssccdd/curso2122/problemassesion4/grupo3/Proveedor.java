/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.ASIGNADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.FABRICACION_NECESARIA;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.FINAL_PEDIDO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.MIN_ORDENADORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.MIN_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.OBJETIVO_CONSTRUCCION;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.PRIMERO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.TIEMPO_ESPERA;
import es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.TipoComponente;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.VARIACION_ORDENADORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.VARIACION_TIEMPO_PROCESADO;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.aleatorio;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.componentes;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Callable<Resultado> {
    private final String iD;
    private final List<Ordenador> listaOrdenadores;
    private final List<Componente> componentesNoAsignados;
    private final ExecutorService ejecucion;
    private final boolean[] fabricacionPendiente;
    private int ordenadoresCompletados;
    private int numOrdenadores;

    public Proveedor(String iD) {
        this.iD = iD;
        this.listaOrdenadores = new ArrayList();
        this.componentesNoAsignados = new ArrayList();
        
        // Genera los ordenadores 
        this.numOrdenadores = MIN_ORDENADORES + aleatorio.nextInt(VARIACION_ORDENADORES);
        this.ordenadoresCompletados = (numOrdenadores * OBJETIVO_CONSTRUCCION) / 100;
        
        // Componentes pendientes de fabricación
        this.fabricacionPendiente = new boolean[componentes.length];
        for(int i = 0; i < componentes.length; i++) 
            fabricacionPendiente[i] = FABRICACION_NECESARIA;
        
        // Crea el marco para la ejecución de los fabricantes
        ejecucion = Executors.newCachedThreadPool();
    }
    
    @Override
    public Resultado call() throws Exception {
        Resultado resultado = null;
        List<Future<List<Componente>>> resultadoFabricacion;
        
        System.out.println("LA TAREA(" + iD + ") comienza la preparación de " +
                           ordenadoresCompletados + " ordenadores completos");
        
        try {
            crearOrdenadores();
            
            // Hasta su finalización o su interrupción
            while( ordenadoresCompletados != FINAL_PEDIDO ) {
                System.out.println("LA TAREA(" + iD + ") espera nuevos componentes");
                resultadoFabricacion = nuevosComponentes();
                
                completarOrdenadores(resultadoFabricacion);   
            }
            
            resultado = new Resultado("Resultado-" + iD, listaOrdenadores, componentesNoAsignados);
            System.out.println("LA TAREA(" + iD + ") ha FINALIZADO");
        } catch(InterruptedException ex) {
            System.out.println("LA TAREA(" + iD + ") ha sido INTERRUMPIDO");
        } finally {
            // finalizamos los gestores activos y el marco de ejecución
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return resultado;
    }

    public String getiD() {
        return iD;
    }
    
    private void crearOrdenadores() { 
        for(int i = 0; i < numOrdenadores; i++) {
            Ordenador ordenador = new Ordenador(iD + "-Ordenador-" + i);
            listaOrdenadores.add(ordenador);
        }
    }

    /**
     * Crea los fabricantes para los componentes que faltan a los ordenadores
     * @return resultao de la fabricación
     * @throws InterruptedException 
     */
    private List<Future<List<Componente>>> nuevosComponentes() throws InterruptedException {
        List<Fabricante> listaFabricantes = new ArrayList();
        
        for(int i = 0; i < fabricacionPendiente.length; i++) {
            TipoComponente tipoComponente = componentes[i];
            if ( fabricacionPendiente[i] ) {
                Fabricante fabricante = new Fabricante("Fabricante-" + iD + "-" + tipoComponente, 
                                                        tipoComponente);
                listaFabricantes.add(fabricante);
            }
        }
        
        return ejecucion.invokeAll(listaFabricantes);
    }
    
    /**
     * Recogemos las diferentes listas de componentes, una a una, y se asignas a 
     * los ordenadores si es posible o se añaden a la lista de descartes.
     * @param resultadoFabricacion todos los componentes fabricados hasta el momento
     * @throws InterruptedException
     * @throws ExecutionException 
     */
    private void completarOrdenadores(List<Future<List<Componente>>> resultadoFabricacion) throws InterruptedException, ExecutionException {
        List<Componente> listaComponentes;
        boolean componenteAsignado;
        Iterator it;
        Componente componente = null;
        int i = 0;
        
        for(Future<List<Componente>> componentesFabricados : resultadoFabricacion) {
            listaComponentes = componentesFabricados.get();
            
            // Asigna el componente al primer ordenador disponible
            componenteAsignado = ASIGNADO;
            it = listaComponentes.iterator();
            while( it.hasNext() && componenteAsignado ) {
                componente = (Componente) it.next();
                componenteAsignado = asignarComponente(componente);
            }
            
            // Si no se ha asignado el componente se añade a la lista de no asignados
            // junto al resto de los fabricados.
            if( !componenteAsignado ) {
                System.out.println("LA TAREA(" + iD + ") no se fabricará más " 
                                       + componente.getTipo());
                while( it.hasNext() ) {
                    componentesNoAsignados.add(componente);
                    componente = (Componente) it.next();
                }
            }
        }  
    }
    
    private boolean asignarComponente(Componente componente) throws InterruptedException {
        boolean asignado = !ASIGNADO;
        int indice = PRIMERO;
        int tiempo;
        
        // Asignamos el componente al primer ordenador posible
        while( (indice < listaOrdenadores.size()) && !asignado ) { 
            Ordenador ordenador = listaOrdenadores.get(indice);
            if( ordenador.addComponente(componente) ) {
                asignado = ASIGNADO;
                if( ordenador.isCompleto() )
                    // Uno menos para finalizar el trabajo
                    ordenadoresCompletados--;
                
                System.out.println("LA TAREA(" + iD + ") ha asignado " + componente +
                                   " al ordenador " + ordenador.getID() + 
                                   " ordenadores pendientes " + ordenadoresCompletados);
            }
            
            indice++;
            
            // Simulamos el tiempo de operación
            tiempo = MIN_TIEMPO_PROCESADO + aleatorio.nextInt(VARIACION_TIEMPO_PROCESADO);
            TimeUnit.SECONDS.sleep(tiempo);
        }
            
        // No se necesita más ese componente
        if( !asignado ) {
            fabricacionPendiente[componente.getTipo().ordinal()] = !FABRICACION_NECESARIA;
            
        }
        
        return asignado;
    }
}
