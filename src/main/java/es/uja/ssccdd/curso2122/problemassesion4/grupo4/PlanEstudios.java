package es.uja.ssccdd.curso2122.problemassesion4.grupo4;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo4.Utils.*;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo4.Utils.TipoRecurso.*;
import java.util.ArrayList;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class PlanEstudios {

    private final int iD;
    private final ArrayList<Recurso> recursos;

    /**
     * Crea un PlanEstudios sin recursos asignados
     *
     * @param iD identificador asociado al plan de estudios
     */
    public PlanEstudios(int iD) {
        this.recursos = new ArrayList<>();
        this.iD = iD;
    }

    public int getiD() {
        return iD;
    }

    /**
     * Asigna un nuevo recurso al plan
     *
     * @param recurso para añadir al pedido, solo se puede añadir si no se han
     * superado los 8 créditos si es asignatura, y si es de otro tipo si no está
     * repetido.
     * @return true si se ha añadido false en caso contrario
     */
    public boolean addRecurso(Recurso recurso) {
        boolean resultado = false;

        switch (recurso.getTipo()) {
            case BLOG://Son casos similares
            case FORO:
                boolean encontradoOtroIgual = false;
                for (Recurso rec : recursos) {
                    if (rec.getTipo().equals(recurso.getTipo())) {
                        encontradoOtroIgual = true;
                    }
                }
                if (!encontradoOtroIgual) {
                    recursos.add(recurso);
                    resultado = true;
                }
                break;

            case ASIGNATURA:
                int creditosActuales = 0;
                for (Recurso rec : recursos) {
                    if (rec.getTipo().equals(ASIGNATURA)) {
                        creditosActuales += rec.getCreditos();
                    }
                }
                if (creditosActuales + recurso.getCreditos() < CREDITOS_ASIGNATURA_MAXIMOS_POR_PLAN) {
                    recursos.add(recurso);
                    resultado = true;
                }
        }

        return resultado;
    }

    public PlanEstudios() {
        this.iD = 0;
        this.recursos = null;
    }

    /**
     * Método para comprobar si el plan está completo
     *
     * @return true si se considera que el plan está completo
     */
    public boolean esCompleto() {

        boolean tieneBlog = false;
        boolean tieneForo = false;
        int creditosAsignatura = 0;

        for (Recurso recurso : recursos) {
            if (recurso.getTipo().equals(FORO)) {
                tieneForo = true;
            }
            if (recurso.getTipo().equals(BLOG)) {
                tieneBlog = true;
            }
            if (recurso.getTipo().equals(ASIGNATURA)) {
                creditosAsignatura += recurso.getCreditos();
            }
        }

        if (tieneForo && tieneBlog && creditosAsignatura >= CREDITOS_ASIGNATURA_PLAN_COMPLETO) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        String resultado = null;

        resultado = "Plan de estudios[" + iD + "] " + (esCompleto() ? "Completado" : "No completado") + " {" + "Recursos=" + recursos + '}';

        return resultado;
    }

}
