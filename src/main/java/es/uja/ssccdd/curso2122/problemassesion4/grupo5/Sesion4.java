package es.uja.ssccdd.curso2122.problemassesion4.grupo5;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Sesion4 {
    public static void main(String[] args) {
        System.out.println("Hilo principal: Iniciando ejecución");
        System.out.println("------------------------------------");
        
        // Creamos todo lo necesario.
        ExecutorService executorDepositos = (ExecutorService) Executors.newFixedThreadPool(Utils.MAX_DEPOSITOS_SIMULTANEAMENTE);

        // - Creación de hilos deposito.
        List<Deposito> depositos = new ArrayList<>();
        for (int i = 0; i < Utils.DEPOSITOS_A_GENERAR; i++) {
            depositos.add(new Deposito(i + 1));
        }

        // - Creación de hilos gestores.
        ArrayList<Reserva> reservas = null;
        ArrayList<Coche> cochesDesechados = new ArrayList<>();

        List<Gestor> gestores = new ArrayList<>();
        for (int i = 0; i < Utils.GESTORES_A_GENERAR; i++) {
            // - Creación de reservas.
            reservas = new ArrayList<>();
            for (int j = 0; j < Utils.RESERVAS_A_GENERAR; j++) {
                int numCoches = Utils.random.nextInt(Utils.MAXIMO_COCHES_POR_RESERVA + 1);
                int valorAleatorio = Utils.random.nextInt(Utils.VALOR_GENERACION);
                reservas.add(new Reserva(j + 1, numCoches, Utils.TipoReserva.getTipoReserva(valorAleatorio)));
            }

            gestores.add(new Gestor(i + 1, cochesDesechados, reservas));
        }

        // Lanzamos depositos y esperamos la devolución de los resultados.
        List<Future<ArrayList<Coche>>> resultados = null;
        try {
            resultados = executorDepositos.invokeAll(depositos);
        } catch (InterruptedException e) {
            System.out.println("Ha ocurrido algún problema al esperar los resultados de los hilos depósito");
        }

        // Asignamos a cada gestor los coches de su depósito.
        ArrayList<Coche> coches = null;
        for (int i = 0; i < resultados.size() && i < gestores.size(); i++) {
            try {
                gestores.get(i).setCochesDisponibles(resultados.get(i).get());
            } catch (ExecutionException | InterruptedException e) {
                System.out.println("Ha ocurrido algún problema al asignar los coches en el gestor " + (i + 1));
            }
        }

        // Lanzamos gestores.
        ExecutorService executorGestores = (ExecutorService) Executors.newFixedThreadPool(Utils.MAX_GESTORES_SIMULTANEAMENTE);
        List<Future<Informe>> informes = null;
        try {
            informes = executorGestores.invokeAll(gestores);
        } catch (InterruptedException e) {
            System.out.println("Ha ocurrido algún problema al esperar el informe");
        }

        // Finalizamos la ejecución del executer.
        executorDepositos.shutdown();
        executorGestores.shutdown();

        // Imprimimos el primer informe que se ha generado.
        for (int i = 0; i < informes.size(); i++) {
            try {
                System.out.println(informes.get(i).get().toString());
            } catch (InterruptedException | ExecutionException ex) {
                System.out.println("Ha ocurrido algún problema al imprimir el informe " + (i + 1));
            }
        }
            
        System.out.println("------------------------------------");
        System.out.println("Hilo principal: Terminando ejecución");
    }
}
