/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo3;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.aleatorio;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.MIN_PROVEEDORES;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo3.Constantes.VARIACION_PROVEEDORES;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        ArrayList<Proveedor> listaProveedores;
        Resultado resultado;
        int numProveedores;
        
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de variables
        ejecucion = Executors.newCachedThreadPool();
        listaProveedores = new ArrayList();
        numProveedores = MIN_PROVEEDORES + aleatorio.nextInt(VARIACION_PROVEEDORES);
        for(int i = 0; i < numProveedores; i++) {
            listaProveedores.add(new Proveedor("Proveedor-" + i));
        }
        
        // Cuerpo de ejecución del hilo principal
        System.out.println("Hilo(PRINCIPAL) comienza la ejecución de los proveedores y espera a "
                + "que finalie el primero");
        resultado = ejecucion.invokeAny(listaProveedores);
        
        // Finaliza el marco de ejecución
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentar resultados
        System.out.println(resultado);
        
        // Finalización
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");

    }
    
}
