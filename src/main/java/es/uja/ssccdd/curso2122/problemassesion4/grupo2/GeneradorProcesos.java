/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo2;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.MIN_TIEMPO_CREACION;
import es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.TipoProceso;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.VARIACION_PROCESOS;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.VARIACION_TIEMPO_CREACION;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo2.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class GeneradorProcesos implements Callable<List<Proceso>> {
    private final String iD;
    private final ArrayList<Proceso> listaProcesos;

    public GeneradorProcesos(String iD) {
        this.iD = iD;
        this.listaProcesos = new ArrayList();
    }

    @Override
    public List<Proceso> call() throws Exception {
        System.out.println("LA TAREA(" + iD + ") comienza su ejecución");
        
        try {
            
            crearProcesos();
            
            System.out.println("LA TAREA(" + iD + ") ha FINALIZADO"); 
        } catch(InterruptedException ex) {
            System.out.println("LA TAREA(" + iD + ") ha sido INTERRUMPIDO");      
        }
        
        return listaProcesos;
    }

    public String getiD() {
        return iD;
    }
    
    private void crearProcesos() throws InterruptedException {
        Proceso proceso;
        int numProcesos;
        
        numProcesos = MIN_PROCESOS + aleatorio.nextInt(VARIACION_PROCESOS);
        for(int i = 0; i < numProcesos; i++) {
            proceso = new Proceso("Proceso-" + iD + "-" + i + "-" + this.hashCode(), 
                                  TipoProceso.getTipoProceso());
            listaProcesos.add(proceso);
 
            TimeUnit.SECONDS.sleep(MIN_TIEMPO_CREACION + aleatorio.nextInt(VARIACION_TIEMPO_CREACION));
        }
        
        System.out.println("LA TAREA(" + iD + ") ha generado " + numProcesos + " procesos");
    }
}
