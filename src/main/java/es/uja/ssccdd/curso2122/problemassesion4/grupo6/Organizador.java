package es.uja.ssccdd.curso2122.problemassesion4.grupo6;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Organizador implements Callable<Informe>{
    // Atributos.
    private final int ID;
    private final ArrayList<Jugador> jugadoresDesechados;
    private final ArrayList<Equipo> equipos;
    private ArrayList<Jugador> jugadoresDisponibles;
  
    // Contadores para generar informe.
    private int numJugadoresAsignados;
    private int numEquiposCompletados;
    private int numJugadoresDesechados;
    
    public Organizador(int ID, ArrayList<Jugador> jugadoresDesechados, 
            ArrayList<Equipo> equipos) {
        this.ID = ID;
        this.jugadoresDesechados = jugadoresDesechados;
        this.equipos = equipos;
        
        this.numJugadoresAsignados = 0;
        this.numEquiposCompletados = 0;
        this.numJugadoresDesechados = 0;
    }
    
    public void setJugadoresDisponibles(ArrayList<Jugador> jugadoresDisponibles) {
        this.jugadoresDisponibles = jugadoresDisponibles;
    }

    private int organizaJugador(int jugadorIndice) {
        int equipoIndice = -1;
        Jugador jugador = jugadoresDisponibles.get(jugadorIndice);
        
        for(int i = 0; i < this.equipos.size() && equipoIndice == -1; i++)
            if (this.equipos.get(i).addJugador(jugador)) {
                equipoIndice = i;
                this.numJugadoresAsignados++;
            }
        
        if (equipoIndice == -1) {
            jugadoresDesechados.add(jugador);
            this.numJugadoresDesechados++;
        } else if (this.equipos.get(equipoIndice).esCompleto())
            this.numEquiposCompletados++;
                    
        return equipoIndice;
    }
    
    @Override
    public Informe call() {
        System.out.println("Organizador " + this.ID + ": ha empezado su ejecución");
        Informe informe = new Informe();
        
        try {   // Si nos interrumpen finalizamos la organización de jugadores,
                // y devolvemos un informe parcial.
                
            // Organización de los jugadores.
            for (int i = 0; i < this.jugadoresDisponibles.size(); i++) {
                int tiempoAleatorio = Utils.random.nextInt(Utils.TIEMPO_ASIGNACION_JUGADORES_MAX - Utils.TIEMPO_ASIGNACION_JUGADORES_MIN) + Utils.TIEMPO_ASIGNACION_JUGADORES_MIN;
                TimeUnit.MILLISECONDS.sleep(tiempoAleatorio);
                this.organizaJugador(i);
            }
        } catch(InterruptedException e) {
            System.out.println("Organizador " + this.ID + ": ha sido interrumpido");
        }

        // Completamos el informe.
        informe.setOrganizadorID(ID);
        informe.setNumEquiposCompletados(this.numEquiposCompletados);
        informe.setNumJugadoresIncluidos(this.numJugadoresAsignados);
        informe.setPorcentajeJugadoresDesechados(this.numJugadoresDesechados / (float) this.jugadoresDisponibles.size() * 100.0f);
        
        System.out.println("Organizador " + this.ID + ": ha terminado su ejecución");
        return informe;
    } 
}
