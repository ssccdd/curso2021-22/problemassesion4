/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo1;

import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.MIN_TIEMPO_REPONEDOR;
import es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.TipoProducto;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.VARIACION_TIEMPO_REPONEDOR;
import static es.uja.ssccdd.curso2122.problemassesion4.grupo1.Constantes.aleatorio;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Reponedor implements Callable<TipoProducto> {
    private final String iD;
    private final TipoProducto tipoProducto;

    public Reponedor(String iD, TipoProducto tipoProducto) {
        this.iD = iD;
        this.tipoProducto = tipoProducto;
    }
    
    @Override
    public TipoProducto call() throws Exception {
        TipoProducto resultado = null;
        
        System.out.println("LA TAREA(" + iD + ") comienza su ejecución");
        
        try {
            resultado = reponeProducto();
            
            System.out.println("LA TAREA(" + iD + ") ha FINALIZADO");
   
        } catch(InterruptedException ex) {
            System.out.println("LA TAREA(" + iD + ") ha sido INTERRUMPIDO");      
        }
        
        return resultado;
    }

    public String getiD() {
        return iD;
    }
    
    private TipoProducto reponeProducto() throws InterruptedException {
        int produccion = MIN_TIEMPO_REPONEDOR + aleatorio.nextInt(VARIACION_TIEMPO_REPONEDOR);
        TimeUnit.SECONDS.sleep(produccion);
        
        return tipoProducto;
    }
}
