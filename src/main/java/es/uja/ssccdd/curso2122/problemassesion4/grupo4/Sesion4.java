package es.uja.ssccdd.curso2122.problemassesion4.grupo4;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Sesion4 {

    public static void main(String[] args) {
        System.out.println("Hilo principal: Iniciando ejecución");
        System.out.println("------------------------------------");

        // Creamos todo lo necesario.
        ExecutorService executorUniversidades = (ExecutorService) Executors.newFixedThreadPool(Utils.MAX_UNIVERSIDADES_SIMULTANEAMENTE);

        // - Creación de hilos universidad.
        List<Universidad> universidades = new ArrayList<>();
        for (int i = 0; i < Utils.UNIVERSIDADES_A_GENERAR; i++) {
            universidades.add(new Universidad(i + 1));
        }

        // - Creación de hilos plataforma.
        ArrayList<PlanEstudios> planesEstudios = null;
        ArrayList<Recurso> recursosDesechados = new ArrayList<>();

        List<PlataformaAprendizaje> plataformas = new ArrayList<>();
        for (int i = 0; i < Utils.PLATAFORMAS_A_GENERAR; i++) {
            // - Creación de planes de estudio.
            planesEstudios = new ArrayList<>();
            for (int j = 0; j < Utils.PLANESTUDIOS_A_GENERAR; j++) {
                planesEstudios.add(new PlanEstudios(j + 1));
            }

            plataformas.add(new PlataformaAprendizaje(i + 1, recursosDesechados, planesEstudios));
        }

        // Lanzamos universidades y esperamos la devolución de los resultados.
        List<Future<ArrayList<Recurso>>> resultados = new ArrayList<>();
        try {
            resultados = executorUniversidades.invokeAll(universidades);
        } catch (InterruptedException e) {
            System.out.println("Ha ocurrido algún problema al esperar los resultados de los hilos universidad");
        }

        // Asignamos a cada plataforma los recursos de su universidad.
        ArrayList<Recurso> recursos = null;
        for (int i = 0; i < resultados.size() && i < plataformas.size(); i++) {
            try {
            plataformas.get(i).setRecursosDisponibles(resultados.get(i).get());
            } catch (ExecutionException | InterruptedException e) {
                System.out.println("Ha ocurrido algún problema al asignar los recursos a la plataforma " + (i + 1));
            }
        }

        // Lanzamos plataformas.
        ExecutorService executorPlataformas = (ExecutorService) Executors.newFixedThreadPool(Utils.PLATAFORMAS_A_GENERAR);
        Informe primer_resultado = null;
        try {
            primer_resultado = executorPlataformas.invokeAny(plataformas);
        } catch (InterruptedException | ExecutionException e) {
            System.out.println("Ha ocurrido algún problema al esperar el informe");
        }

        // Finalizamos la ejecución del executer.
        executorUniversidades.shutdown();
        executorPlataformas.shutdown();

        // Imprimimos el primer informe que se ha generado.
        if (primer_resultado != null)
            System.out.println(primer_resultado.toString());
 
        System.out.println("------------------------------------");
        System.out.println("Hilo principal: Terminando ejecución");
    }
}
