package es.uja.ssccdd.curso2122.problemassesion4.grupo4;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Informe {
    private int plataformaID;
    private int numPlanesCompletados;
    private int numPlanesSinCompletar;
    private float porcentajeRecursosAnyadidos;

    public Informe() {
    }
    
    @Override
    public String toString() {
        return "la plataforma " + plataformaID + " ha conseguido completar " 
                + numPlanesCompletados + " planes de " + (numPlanesSinCompletar + numPlanesCompletados)
                + " y ha conseguido incluir en los planes de estudios el " 
                + porcentajeRecursosAnyadidos + "% de los recursos";
    } 

    /**
     * @param plataformaID the plataformaID to set
     */
    public void setPlataformaID(int plataformaID) {
        this.plataformaID = plataformaID;
    }

    /**
     * @param numPlanesCompletados the numPlanesCompletados to set
     */
    public void setNumPlanesCompletados(int numPlanesCompletados) {
        this.numPlanesCompletados = numPlanesCompletados;
    }

    /**
     * @param numPlanesSinCompletar the numPlanesSinCompletar to set
     */
    public void setNumPlanesSinCompletar(int numPlanesSinCompletar) {
        this.numPlanesSinCompletar = numPlanesSinCompletar;
    }

    /**
     * @param porcentajeRecursosAnyadidos the porcentajeRecursosProcesados to set
     */
    public void setPorcentajeRecursosAnyadidos(float porcentajeRecursosAnyadidos) {
        this.porcentajeRecursosAnyadidos = porcentajeRecursosAnyadidos;
    }
}
