package es.uja.ssccdd.curso2122.problemassesion4.grupo6;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Informe {
    private int organizadorID;
    private int numEquiposCompletados;
    private int numJugadoresIncluidos;
    private float porcentajeJugadoresDesechados;

    public Informe() {
    }
    
    @Override
    public String toString() {
        return "el organizador " + organizadorID + " ha conseguido completar " 
                + numEquiposCompletados + " equipos y ha incluido " + numJugadoresIncluidos
                + " jugadores en equipos. El porcentaje de jugadores desechados es " 
                + porcentajeJugadoresDesechados + "%";
    } 

    /**
     * @param organizadorID the organizadorID to set
     */
    public void setOrganizadorID(int organizadorID) {
        this.organizadorID = organizadorID;
    }

    /**
     * @param numEquiposCompletados the numEquiposCompletados to set
     */
    public void setNumEquiposCompletados(int numEquiposCompletados) {
        this.numEquiposCompletados = numEquiposCompletados;
    }

    /**
     * @param numJugadoresIncluidos the numJugadoresIncluidos to set
     */
    public void setNumJugadoresIncluidos(int numJugadoresIncluidos) {
        this.numJugadoresIncluidos = numJugadoresIncluidos;
    }

    /**
     * @param porcentajeJugadoresDesechados the porcentajeJugadoresDesechados to set
     */
    public void setPorcentajeJugadoresDesechados(float porcentajeJugadoresDesechados) {
        this.porcentajeJugadoresDesechados = porcentajeJugadoresDesechados;
    }
}
