package es.uja.ssccdd.curso2122.problemassesion4.grupo4;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class PlataformaAprendizaje implements Callable<Informe>{
    // Atributos.
    private final int ID;
    private final ArrayList<Recurso> recursosDesechados;
    private final ArrayList<PlanEstudios> planesEstudios;
    private ArrayList<Recurso> recursosDisponibles;
  
    // Contadores para generar informe.
    private int numRecursosAnyadidos;
    private int numPlanesCompletados;
    
    public PlataformaAprendizaje(int ID, ArrayList<Recurso> recursosDesechados, 
            ArrayList<PlanEstudios> planesEstudios) {
        this.ID = ID;
        this.recursosDesechados = recursosDesechados;
        this.planesEstudios = planesEstudios;
        
        this.numRecursosAnyadidos = 0;
        this.numPlanesCompletados = 0;
    }
    
    public void setRecursosDisponibles(ArrayList<Recurso> recursosDisponibles) {
        this.recursosDisponibles = recursosDisponibles;
    }

    private int organizaRecurso(int recursoIndice) {
        int planIndice = -1;
        Recurso recurso = recursosDisponibles.get(recursoIndice);
        
        for(int i = 0; i < this.planesEstudios.size() && planIndice == -1; i++)
            if (this.planesEstudios.get(i).addRecurso(recurso)) {
                planIndice = i;
                this.numRecursosAnyadidos++;
            }
        
        if (planIndice == -1)
            recursosDesechados.add(recurso);
        else if (this.planesEstudios.get(planIndice).esCompleto())
            this.numPlanesCompletados++;
                    
        return planIndice;
    }
    
    @Override
    public Informe call() {
        System.out.println("Plataforma " + this.ID + ": ha empezado su ejecución");
        Informe informe = new Informe();
        
        try {   // Si nos interrumpen finalizamos la organización de recursos,
                //y devolvemos un informe parcial.
                
            // Organización de los recursos.
            for (int i = 0; i < this.recursosDisponibles.size(); i++) {
                int tiempoAleatorio = Utils.random.nextInt(Utils.TIEMPO_ASIGNACION_RECURSOS_MAX - Utils.TIEMPO_ASIGNACION_RECURSOS_MIN) + Utils.TIEMPO_ASIGNACION_RECURSOS_MIN;
                TimeUnit.MILLISECONDS.sleep(tiempoAleatorio);
                this.organizaRecurso(i);
            }
        } catch(InterruptedException e) {
            System.out.println("Plataforma " + this.ID + ": ha sido interrumpido");
        }

        // Completamos el informe.
        informe.setPlataformaID(ID);
        informe.setNumPlanesCompletados(this.numPlanesCompletados);
        informe.setNumPlanesSinCompletar(this.planesEstudios.size() - this.numPlanesCompletados);
        informe.setPorcentajeRecursosAnyadidos(this.numRecursosAnyadidos / (float) this.recursosDisponibles.size() * 100.0f);
        
        System.out.println("Plataforma " + this.ID + ": ha terminado su ejecución");
        return informe;
    } 
}
