package es.uja.ssccdd.curso2122.problemassesion4.grupo4;

import java.util.Random;

/**
 *
 * @author José Luis López Ruiz (llopez)
 */
public class Utils {

    public static Random random = new Random();

    // Constantes del problema
    public static final int PLANESTUDIOS_A_GENERAR = 10;
    public static final int PLATAFORMAS_A_GENERAR = 7;
    public static final int UNIVERSIDADES_A_GENERAR = PLATAFORMAS_A_GENERAR;
    public static final int VALOR_GENERACION = 101; // Valor máximo
    public static final int TOTAL_TIPOS_RECURSOS = TipoRecurso.values().length;
    public static final int RECURSOS_A_GENERAR = 30;
    public static final int CREDITOS_ASIGNATURA_MAXIMOS_POR_PLAN = 8;
    public static final int CREDITOS_ASIGNATURA_PLAN_COMPLETO = 6;
    public static final int CREDITOS_MINIMOS_ASIGNATURA = 3;
    public static final int CREDITOS_MAXIMOS_ASIGNATURA = 6;
    public static final int MAX_UNIVERSIDADES_SIMULTANEAMENTE = 3;
    
    // Variables de tiempo (ms)
    public static final int TIEMPO_CREACION_RECURSOS_MAX = 230;
    public static final int TIEMPO_CREACION_RECURSOS_MIN = 80;
    public static final int TIEMPO_ASIGNACION_RECURSOS_MAX = 700;
    public static final int TIEMPO_ASIGNACION_RECURSOS_MIN = 350;

    //Enumerado para el tipo de plato
    public enum TipoRecurso {
        ASIGNATURA(50), FORO(75), BLOG(100);

        private final int valor;

        private TipoRecurso(int valor) {
            this.valor = valor;
        }

        /**
         * Obtenemos un recurso relacionado con su valor de generación
         *
         * @param valor, entre 0 y 100, de generación del recurso
         * @return el TipoPlato con el valor de generación
         */
        public static TipoRecurso getRecurso(int valor) {
            TipoRecurso resultado = null;
            TipoRecurso[] recursos = TipoRecurso.values();
            int i = 0;

            while ((i < recursos.length) && (resultado == null)) {
                if (recursos[i].valor >= valor) {
                    resultado = recursos[i];
                }

                i++;
            }

            return resultado;
        }
    }

}
