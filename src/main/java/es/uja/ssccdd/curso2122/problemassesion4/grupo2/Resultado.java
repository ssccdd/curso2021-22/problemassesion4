/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso2122.problemassesion4.grupo2;

import java.util.ArrayList;

/**
 *
 * @author pedroj
 */
public class Resultado {
    private final String iD;
    private final RegistroMemoria registro;
    private final ArrayList<Proceso> procesosNoAsignados;
    private final ArrayList<Proceso> procesosFinalizados;

    public Resultado(String iD, RegistroMemoria registro, ArrayList<Proceso> procesosNoAsignados, 
                     ArrayList<Proceso> procesosFinalizados) {
        this.iD = iD;
        this.registro = registro;
        this.procesosNoAsignados = procesosNoAsignados;
        this.procesosFinalizados = procesosFinalizados;
    }

    

    @Override
    public String toString() {
        String resultado  = "************ RESULTADO: " + iD + " *******************" +
                "\n\t Registro memoria:\n\t" + registro + 
                "\n\t Procesos no asignados:\n\t" + procesosNoAsignados + 
                "\n\t Procesos finalizados:\n\t" + procesosFinalizados;
        
        resultado += "\n***************************************************************";
        
        return resultado;
    }
}
